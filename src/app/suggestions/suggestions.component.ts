import { Component, OnInit } from '@angular/core';

import { Suggestion } from '../suggestion';

@Component({
  selector: 'app-suggestions',
  templateUrl: './suggestions.component.html',
  styleUrls: ['./suggestions.component.scss']
})
export class SuggestionsComponent implements OnInit {

  suggestions: Suggestion[] = [];

  newSuggestion: Suggestion = new Suggestion('', 0, false);
  

  constructor() { }

  ngOnInit() {
  }

  onAddSuggestion(): void {
    this.suggestions.push(this.newSuggestion);
    this.newSuggestion = new Suggestion('', 0, false);
  }

  onLike(suggestion: Suggestion): void {
    if (!suggestion.liked) {
      suggestion.likes++;
    } else if (suggestion.likes > 0) {
      suggestion.likes--;
    }
    suggestion.liked = !suggestion.liked;
  }

}
