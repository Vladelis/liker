export class Suggestion {
    title: string;
    likes: number;
    liked: boolean;
    constructor(title: string, likes: number, liked: boolean) { 
        this.title = title; 
        this.likes = likes; 
        this.liked = liked;
    }
}